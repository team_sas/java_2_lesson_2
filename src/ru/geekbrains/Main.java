package ru.geekbrains;

public class Main {

    /**
     * Задача 3
     *
     * В методе main() вызвать полученный метод, обработать возможные исключения MySizeArrayException
     * и MyArrayDataException, и вывести результат расчета.
     *
     * @param args
     */
    public static void main(String[] args) {
        int sum = 0;
        boolean hasErrors = false;
        String[][] numbers = {
            {"1", "2", "3", "4"},
            {"5", "6", "7", "8"},
            {"9", "10", "11", "12"},
            {"13.1", "13,2", "test_1", "*"},
        };
        try {
            sum = sum(numbers);
        } catch (Exception e) {
            // Я осмысленно не отлавливал каждый тип исключения по отдельности, а воспользовался более общим типом.
            hasErrors = true;
            System.out.println("Не удалось определить сумму элементов массива!");
            System.out.println("Исключение " + e.getClass().getSimpleName() + ": " + e.getMessage());
        }
        if (!hasErrors) System.out.println("Сумма элементов массива: " + sum);
    }

    /**
     * Задача 1
     *
     * Напишите метод, на вход которого подаётся двумерный строковый массив размером 4х4,
     * при подаче массива другого размера необходимо бросить исключение MyArraySizeException.
     *
     * Задача 2
     *
     * Далее метод должен пройтись по всем элементам массива, преобразовать в int, и просуммировать.
     * Если в каком-то элементе массива преобразование не удалось (например, в ячейке лежит символ или текст
     * вместо числа), должно быть брошено исключение MyArrayDataException, с детализацией в какой именно ячейке
     * лежат неверные данные.
     *
     * @param arr Массив 4 х 4
     * @return Сумма элементов массива преобразованных к целым числам
     */
    public static int sum(String[][] arr) throws MyArraySizeException, MyArrayDataException {
        int sum = 0;
        if (arr.length != 4 || arr[0].length != 4) {
            throw new MyArraySizeException(
                "Сумма элементов массива может быть определена только для массива 4x4!"
            );
        }
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                String val = arr[i][j];
                try {
                    sum += Integer.parseInt(val);
                } catch (NumberFormatException e) {
                    throw new MyArrayDataException(
                        "Не удалось преобразовать элемент массива в ячейке [" + i + "][" + j + "] со значением '"
                            + val + "' к целому числу!"
                    );
                }
            }
        }
        return sum;
    }
}
