package ru.geekbrains;

public class MyArraySizeException extends Exception {

    MyArraySizeException(String message) {
        super(message);
    }
}
